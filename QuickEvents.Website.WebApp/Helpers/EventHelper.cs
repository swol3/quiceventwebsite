﻿using System.Collections.Generic;
using System.Linq;
using QuickEvents.Website.WebApp.Model.Models;
using QuickEvents.Website.WebApp.ViewModels;

namespace QuickEvents.Website.WebApp.Helpers
{
    public static class EventHelper
    {
        public static Event ConvertEventUploadFormToEvent(this EventUploadForm eventUploadForm)
        {
            var myEvent = new Event
            {
                Id = eventUploadForm.EventID,
                StartDate = eventUploadForm.StartDate,
                EndDate = eventUploadForm.EndDate,
                Name = eventUploadForm.Title,
                Location = eventUploadForm.Location,
                EventImages = ConvertUploadFormImagesToEventImages(eventUploadForm.EventID, eventUploadForm.ImagesAsBase64).ToList()
            };

            return myEvent;
        }

        private static IEnumerable<EventImage> ConvertUploadFormImagesToEventImages(string eventId, IEnumerable<string> imageData)
        {
            return imageData.Select(image => new EventImage {EventID = eventId, ImageAsBase64 = image});
        }
    }
}