﻿using System;

namespace QuickEvents.Website.WebApp.Helpers
{
    public static class StringHelper
    {
        public static string Truncate(this string source, int maxLength)
        {
            return source.Substring(0, Math.Min(maxLength, source.Length));
        }
    }
}