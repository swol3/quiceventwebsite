﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using QuickEvents.Website.WebApp.Model.Models;
using QuickEvents.Website.WebApp.Models;
using QuickEvents.Website.WebApp.Service.Services;
using QuickEvents.Website.WebApp.ViewModels;

namespace QuickEvents.Website.WebApp.Controllers
{
    [RoutePrefix("user")]
    public class UserController : Controller
    {
        [Route]
        [Authorize]
        [HttpGet]
        public ActionResult Index()
        {
            return View();

        }

        [Route("login")]
        public ActionResult Login()
        {
            return PartialView("_Login", new LoginForm());
        }

        [Route("login")]
        [HttpPost]
        public async Task<ActionResult> Login(LoginForm loginForm)
        {
            if (!ModelState.IsValid) return null;

            try
            {
                var user = new QuicEventUser
                {
                    Username = loginForm.UserName,
                    Password = loginForm.Password,
                    EmailAddress = loginForm.Email
                };

                user = await new UserService().GetUser(user);
                if (null != user)
                {
                    FormsAuthentication.SetAuthCookie(loginForm.UserName, false);
                    var userCookie = new HttpCookie("User");
                    userCookie.Values.Add("UserId", $"{user.Id}");
                    Response.Cookies.Add(userCookie);
                    return null;
                }
                FormsAuthentication.SignOut();
                Response.StatusCode = 422;
                ModelState.AddModelError("", "Username or Password is wrong");
                return PartialView("_Login", loginForm);
            }
            catch (Exception ex)
            {

            }

            return null;
        }

        [Route("signout")]
        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [Route("register")]
        public ActionResult Register()
        {
            return PartialView("_RegisterForm", new RegisterForm { IsFromFreeTrial = true });
        }

        [Route("register")]
        [HttpPost]
        public async Task<ActionResult> Register(RegisterForm registerForm)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    var isUsernameFree = await new UserService().CheckAvailability(registerForm.NewUserName);
                    if (isUsernameFree) {
                        var newUser = new QuicEventUser
                        {
                            Username = registerForm.NewUserName,
                            Password = registerForm.NewPassword,
                            EmailAddress = registerForm.NewEmail
                        };

                        newUser = await new UserService().Add(newUser);

                        FormsAuthentication.SetAuthCookie(newUser.Username, false);
                        var userCookie = new HttpCookie("User");
                        userCookie.Values.Add("UserId", $"{newUser.Id}");
                        Response.Cookies.Add(userCookie);
                        return null;
                    }

                    ModelState.AddModelError("NewUserName", "Username already exists.");
                    Response.StatusCode = 422;
                    return PartialView("_RegisterForm", registerForm);

                }
                catch (Exception ex)
                {
                    Response.StatusCode = 400;
                    return null;
                }
            }

            Response.StatusCode = 422;
            return PartialView("_RegisterForm", registerForm);

        }
    }
}