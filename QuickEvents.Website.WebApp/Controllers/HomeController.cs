﻿using System;
using System.Web.Mvc;
using NLog;
using QuickEvents.Website.WebApp.Service.Models;
using QuickEvents.Website.WebApp.Service.Services;

namespace QuickEvents.Website.WebApp.Controllers
{
    [Route("~/", Name = "Default")]
    [RoutePrefix("")]
    public class HomeController : Controller
    {
        [Route("")]
        public ActionResult Index()
        {
            return View();
        }


        [Route("contact")]
        [HttpPost]
        public ActionResult Contact(ContactForm contactForm)
        {
            if (ModelState.IsValid)
            {
                var logger = LogManager.GetCurrentClassLogger();
                try
                {
                    logger.Info($"Sending email from {contactForm.Name} {{{contactForm.Email}}} about \"{contactForm.Subject} \" ");
                    new EmailService().SendContactForm(contactForm);

                    logger.Info("Email Sent!!");
                    return null;
                }
                catch (Exception ex)
                {
                    logger.Error($"{ex.Message}");

                    Response.StatusCode = 400;
                    return null;
                }
            }
            Response.StatusCode = 422;
            return null;

        }
    }

}