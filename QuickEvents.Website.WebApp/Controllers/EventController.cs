﻿using QuickEvents.Website.WebApp.Model.Models;
using QuickEvents.Website.WebApp.Service.Services;
using QuickEvents.Website.WebApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using QuickEvents.Website.WebApp.Helpers;

namespace QuickEvents.Website.WebApp.Controllers
{
    [RoutePrefix("event")]
    public class EventController : Controller
    {
        [Route("")]
        [HttpGet]
        [Authorize]
        public async Task<ActionResult> Index()
        {
            var userCookie = Request.Cookies["User"];
            var events = new List<Event>();

            if (userCookie != null)
            {
                events = await new EventService().GetByUserId(Convert.ToInt32(userCookie.Values["UserId"]));
            }

            return View(events);
        }

        [Route("add")]
        [HttpGet]
        [Authorize]
        public ActionResult Add()
        {
            return View(new EventUploadForm { StartDate = null, EndDate = null });
        }

        [Route("preview")]
        [HttpPost]
        [Authorize]
        public async Task<ActionResult> Preview(EventUploadForm eventUploadForm)
        {
            if (ModelState.IsValid)
            {
                if (await new EventService().CheckAvailability(eventUploadForm.EventID))
                {
                    return PartialView("_Preview", eventUploadForm);
                }
                ModelState.AddModelError("EventID", "This EventID is already taken");
            }
            Response.StatusCode = 422;
            return PartialView("_UploadForm", eventUploadForm);

        }

        [Route("upload")]
        [HttpPost]
        [Authorize]
        public async Task<ActionResult> Upload(EventUploadForm eventUploadForm)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var myEvent = eventUploadForm.ConvertEventUploadFormToEvent();
                    myEvent.UserID = Convert.ToInt32(Request.Cookies["User"].Values["UserId"]);
                    myEvent = await new EventService().Add(myEvent);
                    if (myEvent != null)
                    {
                        Response.StatusCode= 202;
                        return null;
                    }

                    Response.StatusCode = 400;
                    return null;
                }
                catch (Exception ex)
                {
                    Response.StatusCode = 400;
                    return null;
                }
            }
            return null;
        }
    }
}