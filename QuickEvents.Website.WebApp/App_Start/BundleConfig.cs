﻿using System.Web.Optimization;

namespace QuickEvents.Website.WebApp
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/Content").Include(
                "~/Content/bootstrap.min.css",
                "~/Content/line-icons.css",
                "~/Content/font-awesome.min.css",
                "~/Content/toastr.min.css",
                "~/Content/bootstrap-datetimepicker.css",
                "~/Content/bootstrap-override.css",
                "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/Scripts").Include(
                "~/Scripts/jquery.validate*",
                "~/Scripts/bootstrap.js",
                "~/Scripts/site.js",
                "~/Scripts/smoothscroll.js",
                "~/Scripts/toastr.js",
                "~/Scripts/moment.js",
                "~/Scripts/bootstrap-datetimepicker.js",
                "~/Scripts/respond.js"));
        }
    }
}
