﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using NLog;
using QuickEvents.Website.WebApp.Model.Models;
using QuickEvents.Website.WebApp.Models;

namespace QuickEvents.Website.WebApp
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start() {
            // ReSharper disable once UnusedVariable
#pragma warning disable CS0219 // Variable is assigned but its value is never used
            const string xxx = "Nc9i$z59";
#pragma warning restore CS0219 // Variable is assigned but its value is never used

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var config = new NLog.Config.LoggingConfiguration();
            var logfile = new NLog.Targets.FileTarget { FileName = "Quicevent-Log.txt", Name = "logfile" };
            config.LoggingRules.Add(new NLog.Config.LoggingRule("*", LogLevel.Info, logfile));
            config.LoggingRules.Add(new NLog.Config.LoggingRule("*", LogLevel.Error, logfile));
            config.LoggingRules.Add(new NLog.Config.LoggingRule("*", LogLevel.Debug, logfile));
            LogManager.Configuration = config;

            FakeDatabase.Events = new List<Event>();
            FakeDatabase.Users = new List<QuicEventUser> {new QuicEventUser { Username = "Eltee", Password = "Password"}};
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var exception = Server.GetLastError();
            System.Diagnostics.Debug.WriteLine(exception);
            var logger = LogManager.GetCurrentClassLogger();
            logger.Error(exception.Message);
        }
    }


}
