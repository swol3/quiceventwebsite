﻿using System;
using System.Collections.Generic;

namespace QuickEvents.Website.WebApp.Service.Models
{
    public class MetaResult
    {
        public string Message { get; set; }
        public List<Exception> Errors { get; set; }
    }
}
