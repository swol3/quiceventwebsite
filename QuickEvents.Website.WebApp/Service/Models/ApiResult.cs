﻿
namespace QuickEvents.Website.WebApp.Service.Models
{
    public class ApiResult<T>
    {
        public T Data { get; set; }
        public MetaResult Meta { get; set; }
    }
}
