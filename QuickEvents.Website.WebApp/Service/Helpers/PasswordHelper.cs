﻿using System.Security.Cryptography;
using System.Text;
using QuickEvents.Website.WebApp.Model.Models;

namespace QuickEvents.Website.WebApp.Service.Helpers
{
    public static class PasswordHelper
    {
        public static QuicEventUser HashPassword(this QuicEventUser myUser)
        {

            var salt = $"Username={myUser.Username}|~|Password={myUser.Password}|~|Key=QuicEvent;";

            using (var hash = SHA512.Create())
            {
                var hashBytes = hash.ComputeHash(Encoding.UTF8.GetBytes(myUser + salt));
                var stringBuilder = new StringBuilder();

                foreach (var bit in hashBytes)
                {
                    stringBuilder.Append(bit.ToString("x2"));
                }
                myUser.Password = stringBuilder.ToString();
            }
            return myUser;
        }
    }
}
