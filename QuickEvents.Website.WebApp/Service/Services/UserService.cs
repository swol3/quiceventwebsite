﻿using System;
using System.Security.Authentication;
using System.Threading.Tasks;
using QuickEvents.Website.WebApp.Model.Models;
using QuickEvents.Website.WebApp.Service.Helpers;
using QuickEvents.Website.WebApp.Service.Models;

namespace QuickEvents.Website.WebApp.Service.Services
{
    public class UserService
    {
        private string _baseRoute = "user";

        public async  Task<QuicEventUser> GetUser(QuicEventUser user) {
            user.HashPassword();

            var apiCient = new ApiClient<ApiResult<QuicEventUser>>();
            var result = await apiCient.PostAsync($"{_baseRoute}/login", user).ConfigureAwait(false);
            if (null != result.Meta)
            {
                throw new AuthenticationException(result.Meta.Message, result.Meta.Errors[0]);
            }

            return result.Data;
        }

        public async Task<QuicEventUser> Add(QuicEventUser user)
        {
            user.HashPassword();

            var apiClient = new ApiClient<ApiResult<QuicEventUser>>();
            var result = await apiClient.PostAsync($"{_baseRoute}/register", user).ConfigureAwait(false);
            if (null != result.Meta)
            {
                throw new ApplicationException(result.Meta.Message, result.Meta.Errors[0]);
            }

            return result.Data;
        }

        public async Task<bool> CheckAvailability(string username) {
            var apiClient = new ApiClient<ApiResult<bool>>();
            var result = await apiClient.GetAsync($"{_baseRoute}/check/{username}").ConfigureAwait(false);
            if (null != result.Meta)
            {
                throw new ApplicationException(result.Meta.Message, result.Meta.Errors[0]);
            }

            return result.Data;
        }
    }
}
