﻿using System.Net;
using System.Net.Mail;
using QuickEvents.Website.WebApp.Service.Models;

namespace QuickEvents.Website.WebApp.Service.Services
{
    public class EmailService
    {
        public void SendContactForm(ContactForm contactForm)
        {
            const string emailAddress = "b0473n73r741nm3n7@eltee27.com";
            var networkCredential = new NetworkCredential(emailAddress, "Go@ceTeam");

            const string fromAddress = emailAddress;
            var toAddress = contactForm.Email;

            var subject = $"QuicEvent - {contactForm.Subject}";
            var body = $@"{contactForm.Message}

Cheers,
{contactForm.Name}";
            
            SendEmail(networkCredential, fromAddress, toAddress, subject, body);
        }

        private void SendEmail(NetworkCredential networkCredential, string fromAddress, string toAddress, string subject, string body)
        {

            var smtpClient = new SmtpClient
            {
                Host = "mail.eltee27.com",
                Port = 25,
                UseDefaultCredentials = false,
                Credentials = networkCredential
            };

            var message = new MailMessage
            {
                From = new MailAddress(fromAddress)
            };

            message.To.Add(toAddress);
            message.Subject = subject;
            message.Body = body;

            smtpClient.Send(message);

        }
    }
}
