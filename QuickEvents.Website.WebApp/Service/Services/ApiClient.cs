﻿
using System.Net.Http;
using System.Threading.Tasks;

namespace QuickEvents.Website.WebApp.Service.Services
{
    public class ApiClient<T>
    {
        protected HttpClient Client = new HttpClient();
        private readonly string _baseUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["ApiUrl"];

        //todo: Some form of security for requests
        public void PrepareRequest()
        {

        }


        public async Task<T> GetAsync(string route)
        {
            var requestedObject = default(T);

            var response = await Client.GetAsync($"{_baseUrl}/api/{route}").ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                requestedObject =  await response.Content.ReadAsAsync<T>();
            }

            return requestedObject;
        }
        

        public async Task<T> PostAsync(string route, object entity)
        {
            var response = await Client.PostAsJsonAsync($"{_baseUrl}/api/{route}", entity);
            response.EnsureSuccessStatusCode();

            // Deserialize the updated product from the response body.
            var responseEntity = await response.Content.ReadAsAsync<T>();
            return responseEntity;
        }
    }
}
