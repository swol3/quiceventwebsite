﻿using System.Collections.Generic;
using System.Data;
using System.Security.Authentication;
using System.Threading.Tasks;
using QuickEvents.Website.WebApp.Model.Models;
using QuickEvents.Website.WebApp.Service.Models;

namespace QuickEvents.Website.WebApp.Service.Services
{
    public class EventService
    {
        private string _baseRoute = "/event";

        public async Task<List<Event>> GetByUserId(int Id)
        {
            var apiCient = new ApiClient<ApiResult<List<Event>>>();
            var result = await apiCient.GetAsync($"{_baseRoute}/user/{Id}").ConfigureAwait(false);
            if (null != result.Meta)
            {
                throw new AuthenticationException(result.Meta.Message, result.Meta.Errors[0]);
            }

            return result.Data;
        }

        public async Task<bool> CheckAvailability(string eventId)
        {
            var apiClient = new ApiClient<ApiResult<bool>>();
            var result = await apiClient.GetAsync($"{_baseRoute}/check/{eventId}").ConfigureAwait(false);
            if (null != result.Meta)
            {
                throw new DuplicateNameException(result.Meta.Message, result.Meta.Errors[0]);
            }

            return result.Data;
        }

        public async Task<Event> Add(Event myEvent)
        {
            var apiClient = new ApiClient<ApiResult<Event>>();
            var result = await apiClient.PostAsync($"{_baseRoute}/", myEvent).ConfigureAwait(false);
            if (null != result.Meta)
            {
                throw new DuplicateNameException(result.Meta.Message, result.Meta.Errors[0]);
            }

            return result.Data;
        }
    }
}
