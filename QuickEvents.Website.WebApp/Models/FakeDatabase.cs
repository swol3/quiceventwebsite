﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QuickEvents.Website.WebApp.Model.Models;

namespace QuickEvents.Website.WebApp.Models
{
    public static class FakeDatabase
    {
        public static List<Event> Events { get; set; }
        public static List<QuicEventUser> Users { get; set; }

    }
}