﻿
using System.ComponentModel.DataAnnotations;

namespace QuickEvents.Website.WebApp.ViewModels
{
    public class LoginForm
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [RegularExpression(@"(([\w-\.]+)@([\w-\.]+)\.([\w-\.]+))")]
        public string Email { get; set; }
    }
}