﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace QuickEvents.Website.WebApp.ViewModels
{
    public class EventUploadForm
    {
        [Required]
        public string EventID { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Location { get; set; }
        //[Required]
        public DateTime? StartDate { get; set; }
        //[Required]
        public DateTime? EndDate { get; set; }
        [Required]
        public IEnumerable<string> ImagesAsBase64 { get; set; }
    }
}