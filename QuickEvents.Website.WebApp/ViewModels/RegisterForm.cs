﻿using System.ComponentModel.DataAnnotations;

namespace QuickEvents.Website.WebApp.ViewModels
{
    public class RegisterForm
    {
        [Required]
        public string NewUserName { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }
        [Compare("NewPassword")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
        [Required]
        [RegularExpression(@"(([\w-\.]+)@([\w-\.]+)\.([\w-\.]+))")]
        public string NewEmail { get; set; }

        public bool IsFromFreeTrial { get; set; }
    }
}