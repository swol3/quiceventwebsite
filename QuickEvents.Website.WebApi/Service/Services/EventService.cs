﻿using System.Collections.Generic;
using Data.Repositories;
using QuickEvents.Website.WebApi.Model;

namespace QuickEvents.Website.WebApi.Service.Services
{
    public class EventService : BaseService
    {
        private readonly EventRepository _repo;

        public EventService()
        {
            _repo = new EventRepository(Context);
        }

        public EventService(QuicEventEntities context)
        {
            _repo = new EventRepository(context);
        }

        public Event Add(Event newEvent) {
            var createdEvent = _repo.Add(newEvent);
            using (var eis = new EventImageService(Context))
            {
                createdEvent.EventImages = new List<EventImage>();
                foreach (var eventImage in newEvent.EventImages)
                {
                    createdEvent.EventImages.Add(eis.Add(eventImage));
                }
            }

            return newEvent;
        }

        public Event Get(string id) {
            return _repo.Get(id);
        }

        public bool CheckAvailability(string id)
        {
            return _repo.CheckAvailability(id);
        }

        public Event GetWithRelated(string id) {
                var theEvent = _repo.Get(id);
                new EventItemService(Context).GetByEventId(id);
                new EventImageService(Context).GetByEventId(id);

                return theEvent;
        }

        public List<Event> GetByUserId(int id) {
            return _repo.GetByUserId(id);
        }

        public Event Update(Event updatedEvent) {
            return _repo.Update(updatedEvent);
        }

        public void Delete(string id)
        {
            new EventItemService().DeleteAllByEvent(id);
            _repo.Delete(id);
        }

        public void DeleteAllByUser(int id)
        {
            _repo.DeleteAllByUser(id);
        }
    }
}
