﻿using Data.Repositories;
using QuickEvents.Website.WebApi.Model;

namespace QuickEvents.Website.WebApi.Service.Services
{
    public class UserService : BaseService
    {
        private readonly UserRepository _repo;

        public UserService()
        {
            _repo = new UserRepository(Context);
        }

        public QuicEventUser Add(QuicEventUser user) {
            return _repo.Add(user);
        }

        public QuicEventUser Get(int id) {
            return _repo.Get(id);
        }

        public QuicEventUser Get(QuicEventUser user) {
            return _repo.Get(user);
        }

        public QuicEventUser Update(QuicEventUser user) {
            return _repo.Update(user);
        }

        public bool CheckAvailability(string username) {
            return _repo.CheckAvailability(username);
        }

        public void Delete(int id)
        {
            _repo.Delete(id);
        }
    }
}
