﻿using Data.Repositories;
using QuickEvents.Website.WebApi.Model;

namespace QuickEvents.Website.WebApi.Service.Services
{
    public class PriceModelService : BaseService
    {
        private readonly PriceModelRepository _repo;

        public PriceModelService()
        {
            _repo = new PriceModelRepository(Context);
        }

        public PriceModel Add(PriceModel priceModel) {
            return _repo.Add(priceModel);
        }

        public PriceModel Get(int id) {
            return _repo.Get(id);
        }

        public PriceModel Update(PriceModel priceModel) {
            return _repo.Update(priceModel);
        }

        public void Delete(int id)
        {
            _repo.Delete(id);
        }
    }
}
