﻿using System.Collections.Generic;
using Data.Repositories;
using QuickEvents.Website.WebApi.Model;

namespace QuickEvents.Website.WebApi.Service.Services
{
    public class EventImageService : BaseService
    {
        private readonly EventImageRepository _repo;

        public EventImageService()
        {
            _repo = new EventImageRepository(Context);
        }

        public EventImageService(QuicEventEntities context)
        {
            _repo = new EventImageRepository(context);
        }

        public EventImage Add(EventImage eventImage) {
            return _repo.Add(eventImage);
        }

        public EventImage Get(int id) {
            return _repo.Get(id);
        }

        public List<EventImage> GetByEventId(string id) {
            var x = _repo.GetByEventId(id);
            return x;
        }

        public EventImage Update(EventImage eventImage) {
            return _repo.Update(eventImage);
        }

        public void Delete(int id)
        {
            _repo.Delete(id);
        }

        public void DeleteAllByEvent(string id)
        {
            _repo.DeleteAllByEvent(id);
        }
    }
}
