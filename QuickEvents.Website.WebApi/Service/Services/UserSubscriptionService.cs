﻿using System.Collections.Generic;
using Data.Repositories;
using QuickEvents.Website.WebApi.Model;

namespace QuickEvents.Website.WebApi.Service.Services
{
    public class UserSubscriptionService : BaseService
    {
        private readonly UserSubscriptionRepository _repo;

        public UserSubscriptionService()
        {
            _repo = new UserSubscriptionRepository(Context);
        }

        public UserSubscription Add(UserSubscription newUserSubscription) {
            return _repo.Add(newUserSubscription);
        }

        public UserSubscription Get(int id) {
            return _repo.Get(id);
        }

        public UserSubscription GetWithRelated(int id) {
            var theUserSubscription =_repo.Get(id);
            if (theUserSubscription.UserID != null) {
                new PaymentHistoryService().GetByUserId((int) theUserSubscription.UserID);
            }

            return theUserSubscription;
        }

        public List<UserSubscription> GetByUserId(int id) {
            return _repo.GetByUserId(id);
        }

        public UserSubscription Update(UserSubscription updatedUserSubscription) {
            return _repo.Update(updatedUserSubscription);
        }

        public void Delete(int id) {
            var uss = Get(id);
            if (uss.UserID != null) {
                new PaymentHistoryService().DeleteAllByUser((int) uss.UserID);
            }
            _repo.Delete(id);
        }

        public void DeleteAllByUser(int id)
        {
            _repo.DeleteAllByUser(id);
        }
    }
}
