﻿using System.Collections.Generic;
using Data.Repositories;
using QuickEvents.Website.WebApi.Model;

namespace QuickEvents.Website.WebApi.Service.Services
{
    public class PaymentHistoryService : BaseService
    {
        private readonly PaymentHistoryRepository _repo;

        public PaymentHistoryService()
        {
            _repo = new PaymentHistoryRepository(Context);
        }

        public PaymentHistory Add(PaymentHistory newPaymentHistory) {
            return _repo.Add(newPaymentHistory);
        }

        public PaymentHistory Get(int id) {
            return _repo.Get(id);
        }

        public PaymentHistory GetWithRelated(int id) {
            var thePaymentHistory =_repo.Get(id);
            if (thePaymentHistory.UserID != null)
            {
                new PaymentHistoryService().GetByUserId((int)thePaymentHistory.UserID);
            }

            return thePaymentHistory;
        }

        public List<PaymentHistory> GetByUserId(int id) {
            return _repo.GetByUserId(id);
        }

        public PaymentHistory Update(PaymentHistory updatedPaymentHistory) {
            return _repo.Update(updatedPaymentHistory);
        }

        public void Delete(int id)
        {
            var uss = Get(id);
            if (uss.UserID != null)
            {
                new PaymentHistoryService().DeleteAllByUser((int)uss.UserID);
            }
            _repo.Delete(id);
        }

        public void DeleteAllByUser(int id)
        {
            _repo.DeleteAllByUser(id);
        }
    }
}
