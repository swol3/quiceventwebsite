﻿using System;
using QuickEvents.Website.WebApi.Model;

namespace QuickEvents.Website.WebApi.Service.Services
{
    public class BaseService : IDisposable
    {
        protected readonly QuicEventEntities Context = new QuicEventEntities();


        public void Dispose()
        {
            Context.SaveChanges();
            Context.Dispose();
        }
    }
}
