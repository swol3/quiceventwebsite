﻿using System.Collections.Generic;
using Data.Repositories;
using QuickEvents.Website.WebApi.Model;

namespace QuickEvents.Website.WebApi.Service.Services
{
    public class EventItemService : BaseService
    {
        private readonly EventItemRepository _repo;

        public EventItemService()
        {
            _repo = new EventItemRepository(Context);
        }

        public EventItemService(QuicEventEntities context)
        {
            _repo = new EventItemRepository(context);
        }

        public EventItem Add(EventItem eventItem) {
            return _repo.Add(eventItem);
        }

        public EventItem Get(int id) {
            return _repo.Get(id);
        }

        public List<EventItem> GetByEventId(string id) {
            return _repo.GetByEventId(id);
        }

        public EventItem Update(EventItem eventItem) {
            return _repo.Update(eventItem);
        }

        public void Delete(int id)
        {
            _repo.Delete(id);
        }

        public void DeleteAllByEvent(string id)
        {
            _repo.DeleteAllByEvent(id);
        }
    }
}
