﻿namespace QuickEvents.Website.WebApi.ResponseObjects
{
    public class ApiResult<T>
    {
        public T Data { get; set; }
        public MetaResult Meta { get; set; }
    }
}