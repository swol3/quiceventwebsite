﻿using System;
using System.Collections.Generic;

namespace QuickEvents.Website.WebApi.ResponseObjects
{
    public class MetaResult
    {
        public string Message { get; set; }
        public List<Exception> Errors { get; set; }
    }
}