﻿using System.Linq;
using QuickEvents.Website.WebApi.Model;

namespace Data.Repositories
{
    public class UserRepository
    {
        private readonly QuicEventEntities _db;

        public UserRepository(QuicEventEntities db)
        {
            _db = db;
        }

        public QuicEventUser Add(QuicEventUser newUser) => _db.Insert_Users(newUser.FirstName, newUser.LastName, newUser.Address,
                newUser.Username, newUser.EmailAddress, newUser.Password).SingleOrDefault();

        public QuicEventUser Get(QuicEventUser user) => _db.FindUserByUsernameAndPassword(user.Username, user.Password).SingleOrDefault();

        public QuicEventUser Get(int id) => _db.Select_Users(id).SingleOrDefault();

        public QuicEventUser Update(QuicEventUser updatedUser) => _db.Update_Users(updatedUser.Id, updatedUser.FirstName, updatedUser.LastName, updatedUser.Address,
                updatedUser.Username, updatedUser.EmailAddress, updatedUser.Password).SingleOrDefault();

        public bool CheckAvailability(string username) => _db.CheckUserAvailability(username).SingleOrDefault()?? true;

        public void Delete(int id) => _db.Delete_Users(id);
    }
}
