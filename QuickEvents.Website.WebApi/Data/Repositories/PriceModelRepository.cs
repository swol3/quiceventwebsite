﻿using System.Linq;
using QuickEvents.Website.WebApi.Model;

namespace Data.Repositories
{
    public class PriceModelRepository
    {
        private readonly QuicEventEntities _db;

        public PriceModelRepository(QuicEventEntities db)
        {
            _db = db;
        }

        public PriceModel Add(PriceModel priceModel)
        {
            return _db.Insert_PriceModel(priceModel.Region, priceModel.Price).SingleOrDefault();
        }

        public PriceModel Get(int id)
        {
            return _db.Select_PriceModel(id).SingleOrDefault();
        }

        public PriceModel Update(PriceModel priceModel) {
            return _db.Update_PriceModel(priceModel.Id,priceModel.Region, priceModel.Price).SingleOrDefault();
        }

        public void Delete(int id) {
           _db.Delete_PriceModel(id);
        }
    }
}
