﻿using System.Collections.Generic;
using System.Linq;
using QuickEvents.Website.WebApi.Model;

namespace Data.Repositories
{
    public class PaymentHistoryRepository
    {
        private readonly QuicEventEntities _db;

        public PaymentHistoryRepository(QuicEventEntities db)
        {
            _db = db;
        }

        public PaymentHistory Add(PaymentHistory paymentHistory)
        {
            return _db.Insert_PaymentHistory(paymentHistory.UserID, paymentHistory.MaskedCreditCard, 
                paymentHistory.TransactionDate).SingleOrDefault();
        }

        public PaymentHistory Get(int id)
        {
            return _db.Select_PaymentHistory(id).SingleOrDefault();
        }

        public List<PaymentHistory> GetByUserId(int id)
        {
            return _db.Select_PaymentHistoryByUserID(id).ToList();
        }

        public PaymentHistory Update(PaymentHistory paymentHistory) {
            return _db.Update_PaymentHistory(paymentHistory.Id, paymentHistory.UserID, paymentHistory.MaskedCreditCard,
                paymentHistory.TransactionDate).SingleOrDefault();
        }

        public void Delete(int id) {
           _db.Delete_PaymentHistory(id);
        }

        public void DeleteAllByUser(int id) {
            _db.Delete_PaymentHistoryByUserID(id);
        }
    }
}
