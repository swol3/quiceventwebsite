﻿using System.Collections.Generic;
using System.Linq;
using QuickEvents.Website.WebApi.Model;

namespace Data.Repositories
{
    public class EventRepository
    {
        private readonly QuicEventEntities _db;

        public EventRepository(QuicEventEntities db)
        {
            _db = db;
        }

        public Event Add(Event newEvent)
        {
            return _db.Insert_Event(newEvent.Id, newEvent.UserID, newEvent.Name, newEvent.ContainsScheduleITems,
                newEvent.StartDate, newEvent.EndDate, newEvent.Location).SingleOrDefault();
        }

        public Event Get(string id)
        {
            return _db.Select_Event(id).SingleOrDefault();
        }

        public bool CheckAvailability(string id)
        {
            return _db.CheckEventAvailability(id).FirstOrDefault()??false;
        }

        public List<Event> GetByUserId(int id)
        {
            return _db.Select_EventByUserID(id).ToList();
        }

        public Event Update(Event updatedEvent) {
            return _db.Update_Event(updatedEvent.Id, updatedEvent.UserID, updatedEvent.Name, updatedEvent.ContainsScheduleITems,
                updatedEvent.StartDate, updatedEvent.EndDate, updatedEvent.Location).SingleOrDefault();
        }

        public void Delete(string id) {
           _db.Delete_Event(id);
        }

        public void DeleteAllByUser(int id) {
            _db.Delete_EventByUserID(id);
        }
    }
}
