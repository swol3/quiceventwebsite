﻿using System.Collections.Generic;
using System.Linq;
using QuickEvents.Website.WebApi.Model;

namespace Data.Repositories
{
    public class EventImageRepository
    {
        private readonly QuicEventEntities _db;

        public EventImageRepository(QuicEventEntities db)
        {
            _db = db;
        }

        public EventImage Add(EventImage EventImage)
        {
            return _db.Insert_EventImage(EventImage.EventID, EventImage.ImageAsBase64).SingleOrDefault();
        }

        public EventImage Get(int id)
        {
            return _db.Select_EventImage(id).SingleOrDefault();
        }

        public List<EventImage> GetByEventId(string id)
        {
            return _db.Select_EventImagesByEventID(id).ToList();
        }

        public EventImage Update(EventImage EventImage) {
            return _db.Update_EventImage(EventImage.Id,EventImage.EventID, EventImage.ImageAsBase64).SingleOrDefault();
        }

        public void Delete(int id) {
            _db.Delete_EventImage(id); 
        }

        public void DeleteAllByEvent(string id) {
            _db.Delete_EventImagesByEventID(id);
        }
    }
}
