﻿using System.Collections.Generic;
using System.Linq;
using QuickEvents.Website.WebApi.Model;

namespace Data.Repositories
{
    public class EventItemRepository
    {
        private readonly QuicEventEntities _db;

        public EventItemRepository(QuicEventEntities db)
        {
            _db = db;
        }

        public EventItem Add(EventItem eventItem)
        {
            return _db.Insert_EventItem(eventItem.EventID, eventItem.Name, eventItem.StartDate, eventItem.EndDate).SingleOrDefault();
        }

        public EventItem Get(int id)
        {
            return _db.Select_EventItem(id).SingleOrDefault();
        }

        public List<EventItem> GetByEventId(string id)
        {
            return _db.Select_EventItemByEventID(id).ToList();
        }

        public EventItem Update(EventItem eventItem) {
            return _db.Update_EventItem(eventItem.Id,eventItem.EventID, eventItem.Name, eventItem.StartDate, eventItem.EndDate).SingleOrDefault();
        }

        public void Delete(int id) {
            _db.Delete_EventItem(id); 
        }

        public void DeleteAllByEvent(string id) {
            _db.Delete_EventItemByEventID(id);
        }
    }
}
