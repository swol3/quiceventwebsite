﻿using System.Collections.Generic;
using System.Linq;
using QuickEvents.Website.WebApi.Model;

namespace Data.Repositories
{
    public class UserSubscriptionRepository
    {
        private readonly QuicEventEntities _db;

        public UserSubscriptionRepository(QuicEventEntities db)
        {
            _db = db;
        }

        public UserSubscription Add(UserSubscription userSubscription)
        {
            return _db.Insert_UserSubscription(userSubscription.UserID, userSubscription.PaymentHistoryID, userSubscription.StartDate,
                userSubscription.EndDate).SingleOrDefault();
        }

        public UserSubscription Get(int id)
        {
            return _db.Select_UserSubscription(id).SingleOrDefault();
        }

        public List<UserSubscription> GetByUserId(int id)
        {
            return _db.Select_UserSubscriptionByUserID(id).ToList();
        }

        public UserSubscription Update(UserSubscription userSubscription) {
            return _db.Update_UserSubscription(userSubscription.Id,userSubscription.UserID, userSubscription.PaymentHistoryID, userSubscription.StartDate,
                userSubscription.EndDate).SingleOrDefault();
        }

        public void Delete(int id) {
           _db.Delete_UserSubscription(id);
        }

        public void DeleteAllByUser(int id) {
            _db.Delete_UserSubscriptionByUserID(id);
        }
    }
}
