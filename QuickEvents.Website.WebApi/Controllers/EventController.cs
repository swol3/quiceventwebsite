﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using QuickEvents.Website.WebApi.Model;
using QuickEvents.Website.WebApi.ResponseObjects;
using QuickEvents.Website.WebApi.Service.Services;

namespace QuickEvents.Website.WebApi.Controllers
{
    [RoutePrefix("api/event")]
    public class EventController : ApiController
    {
        [Route("{id}")]
        [HttpGet]
        public IHttpActionResult Get(string id)
        {
            try
            {
                using (var es = new EventService())
                {
                    var myEvent = es.Get(id);

                    var result = new ApiResult<Event>
                    {
                        Data = myEvent
                    };

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("{id}/withrelated")]
        [HttpGet]
        public IHttpActionResult GetWithRelated(string id)
        {
            try
            {
                using (var es = new EventService())
                {
                    var myEvent = es.GetWithRelated(id);

                    var result = new ApiResult<Event>
                    {
                        Data = myEvent
                    };

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("check/{id}")]
        [HttpGet]
        public IHttpActionResult CheckAvailability(string id)
        {
            try
            {
                using (var es = new EventService())
                {
                    var isAvailable = es.CheckAvailability(id);
                    var result = new ApiResult<bool>
                    {
                        Data = isAvailable
                    };

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("user/{id}")]
        [HttpGet]
        public IHttpActionResult GetByUser(int id)
        {
            try
            {
                using (var es = new EventService())
                {
                    var myEvent = es.GetByUserId(id);

                    var result = new ApiResult<List<Event>>
                    {
                        Data = myEvent
                    };

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult Add(Event newEvent)
        {
            try
            {
                using (var es = new EventService())
                {
                    var myEvent = es.Add(newEvent);

                    var result = new ApiResult<Event>
                    {
                        Data = myEvent
                    };

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("update")]
        [HttpPost]
        public IHttpActionResult Update(Event newEvent)
        {
            try
            {
                using (var es = new EventService())
                {
                    var myEvent = es.Add(newEvent);

                    var result = new ApiResult<Event>
                    {
                        Data = myEvent
                    };

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("delete/{id}")]
        [HttpDelete]
        public IHttpActionResult Delete(string id)
        {
            try
            {
                using (var es = new EventService())
                {
                    es.Delete(id);

                    return Ok();
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("delete/user/{id}")]
        [HttpDelete]
        public IHttpActionResult DeleteByUser(int id)
        {
            try
            {
                using (var es = new EventService())
                {
                    es.DeleteAllByUser(id);

                    return Ok();
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }
    }
}