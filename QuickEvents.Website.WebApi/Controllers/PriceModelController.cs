﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using QuickEvents.Website.WebApi.Model;
using QuickEvents.Website.WebApi.ResponseObjects;
using QuickEvents.Website.WebApi.Service.Services;

namespace QuickEvents.Website.WebApi.Controllers
{
    [RoutePrefix("api/priceModel")]
    public class PriceModelController : ApiController
    {
        [Route("{id}")]
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                using (var userService = new PriceModelService())
                {
                    var myEvent = userService.Get(id);

                    var result = new ApiResult<PriceModel>
                    {
                        Data = myEvent
                    };

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }
        
        [Route("add")]
        [HttpPost]
        public IHttpActionResult Add(PriceModel priceModel)
        {
            try
            {
                using (var priceModelService = new PriceModelService())
                {
                    var myEvent = priceModelService.Add(priceModel);

                    var result = new ApiResult<PriceModel>
                    {
                        Data = myEvent
                    };

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("update")]
        [HttpPost]
        public IHttpActionResult Update(PriceModel priceModel)
        {
            try
            {
                using (var priceModelService = new PriceModelService())
                {
                    var myEvent = priceModelService.Add(priceModel);

                    var result = new ApiResult<PriceModel>
                    {
                        Data = myEvent
                    };

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("delete/{id}")]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                using (var priceModelService = new PriceModelService())
                {
                    priceModelService.Delete(id);

                    return Ok();
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }
    }
}