﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using QuickEvents.Website.WebApi.Model;
using QuickEvents.Website.WebApi.ResponseObjects;
using QuickEvents.Website.WebApi.Service.Services;

namespace QuickEvents.Website.WebApi.Controllers
{
    [RoutePrefix("api/user")]
    public class UserController : ApiController
    {
        [Route("login")]
        [HttpPost]
        public IHttpActionResult ValidateLogin(QuicEventUser user)
        {
            try
            {
                using (var userService = new UserService())
                {
                    var myEvent = userService.Get(user);

                    var result = new ApiResult<QuicEventUser>
                    {
                        Data = myEvent
                    };

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }


        [Route("{id}")]
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                using (var userService = new UserService())
                {
                    var myEvent = userService.Get(id);

                    var result = new ApiResult<QuicEventUser>
                    {
                        Data = myEvent
                    };

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("check/{username}")]
        [HttpGet]
        public IHttpActionResult CheckAvailability(string username)
        {
            try
            {
                using (var userService = new UserService())
                {
                    var myEvent = userService.CheckAvailability(username);

                    var result = new ApiResult<bool>
                    {
                        Data = myEvent
                    };

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("register")]
        [HttpPost]
        public IHttpActionResult Add(QuicEventUser user)
        {
            try
            {
                using (var userService = new UserService())
                {
                    var myEvent = userService.Add(user);

                    var result = new ApiResult<QuicEventUser>
                    {
                        Data = myEvent
                    };

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("update")]
        [HttpPost]
        public IHttpActionResult Update(QuicEventUser user)
        {
            try
            {
                using (var userService = new UserService())
                {
                    var myEvent = userService.Add(user);

                    var result = new ApiResult<QuicEventUser>
                    {
                        Data = myEvent
                    };

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("delete/{id}")]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                using (var userService = new UserService())
                {
                    userService.Delete(id);

                    return Ok();
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }
    }
}