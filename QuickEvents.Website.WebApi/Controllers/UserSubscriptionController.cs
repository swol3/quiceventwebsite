﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using QuickEvents.Website.WebApi.Model;
using QuickEvents.Website.WebApi.ResponseObjects;
using QuickEvents.Website.WebApi.Service.Services;

namespace QuickEvents.Website.WebApi.Controllers
{
    [RoutePrefix("api/usersubscription")]
    public class UserSubscriptionController : ApiController
    {
        [Route("{id}")]
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                using (var es = new UserSubscriptionService())
                {
                    var myUserSubscription = es.Get(id);

                    var result = new ApiResult<UserSubscription>
                    {
                        Data = myUserSubscription
                    };

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("user/{id}")]
        [HttpGet]
        public IHttpActionResult GetByUser(int id)
        {
            try
            {
                using (var es = new UserSubscriptionService())
                {
                    var myUserSubscription = es.GetByUserId(id);

                    var result = new ApiResult<List<UserSubscription>>
                    {
                        Data = myUserSubscription
                    };

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("add")]
        [HttpPost]
        public IHttpActionResult Add(UserSubscription newUserSubscription)
        {
            try
            {
                using (var es = new UserSubscriptionService())
                {
                    var myUserSubscription = es.Add(newUserSubscription);

                    var result = new ApiResult<UserSubscription>
                    {
                        Data = myUserSubscription
                    };

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("update")]
        [HttpPost]
        public IHttpActionResult Update(UserSubscription newUserSubscription)
        {
            try
            {
                using (var es = new UserSubscriptionService())
                {
                    var myUserSubscription = es.Add(newUserSubscription);

                    var result = new ApiResult<UserSubscription>
                    {
                        Data = myUserSubscription
                    };

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("delete/{id}")]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                using (var es = new UserSubscriptionService())
                {
                    es.Delete(id);

                    return Ok();
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("delete/user/{id}")]
        [HttpDelete]
        public IHttpActionResult DeleteByUser(int id)
        {
            try
            {
                using (var es = new UserSubscriptionService())
                {
                    es.DeleteAllByUser(id);

                    return Ok();
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }
    }
}