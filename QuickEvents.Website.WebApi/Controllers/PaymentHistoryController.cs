﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using QuickEvents.Website.WebApi.Model;
using QuickEvents.Website.WebApi.ResponseObjects;
using QuickEvents.Website.WebApi.Service.Services;

namespace QuickEvents.Website.WebApi.Controllers
{
    [RoutePrefix("api/paymenthistory")]
    public class PaymentHistoryController : ApiController
    {
        [Route("{id}")]
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                using (var es = new PaymentHistoryService())
                {
                    var myPaymentHistory = es.Get(id);

                    var result = new ApiResult<PaymentHistory>
                    {
                        Data = myPaymentHistory
                    };

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("user/{id}")]
        [HttpGet]
        public IHttpActionResult GetByUser(int id)
        {
            try
            {
                using (var es = new PaymentHistoryService())
                {
                    var myPaymentHistory = es.GetByUserId(id);

                    var result = new ApiResult<List<PaymentHistory>>
                    {
                        Data = myPaymentHistory
                    };

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("add")]
        [HttpPost]
        public IHttpActionResult Add(PaymentHistory newPaymentHistory)
        {
            try
            {
                using (var es = new PaymentHistoryService())
                {
                    var myPaymentHistory = es.Add(newPaymentHistory);

                    var result = new ApiResult<PaymentHistory>
                    {
                        Data = myPaymentHistory
                    };

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("update")]
        [HttpPost]
        public IHttpActionResult Update(PaymentHistory newPaymentHistory)
        {
            try
            {
                using (var es = new PaymentHistoryService())
                {
                    var myPaymentHistory = es.Add(newPaymentHistory);

                    var result = new ApiResult<PaymentHistory>
                    {
                        Data = myPaymentHistory
                    };

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("delete/{id}")]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                using (var es = new PaymentHistoryService())
                {
                    es.Delete(id);

                    return Ok();
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("delete/user/{id}")]
        [HttpDelete]
        public IHttpActionResult DeleteByUser(int id)
        {
            try
            {
                using (var es = new PaymentHistoryService())
                {
                    es.DeleteAllByUser(id);

                    return Ok();
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }
    }
}