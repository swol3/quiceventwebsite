﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using QuickEvents.Website.WebApi.Model;
using QuickEvents.Website.WebApi.ResponseObjects;
using QuickEvents.Website.WebApi.Service.Services;

namespace QuickEvents.Website.WebApi.Controllers
{
    [RoutePrefix("api/eventitem")]
    public class EventItemController : ApiController
    {
        [Route("{id}")]
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                using (var es = new EventItemService())
                {
                    var myEvent = es.Get(id);

                    var result = new ApiResult<EventItem>
                    {
                        Data = myEvent
                    };

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("event/{id}")]
        [HttpGet]
        public IHttpActionResult GetByEvent(string id)
        {
            try
            {
                using (var es = new EventItemService())
                {
                    var myEvent = es.GetByEventId(id);

                    var result = new ApiResult<List<EventItem>>
                    {
                        Data = myEvent
                    };

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("add")]
        [HttpPost]
        public IHttpActionResult Add(EventItem eventItem)
        {
            try
            {
                using (var es = new EventItemService())
                {
                    var myEvent = es.Add(eventItem);

                    var result = new ApiResult<EventItem>
                    {
                        Data = myEvent
                    };

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("update")]
        [HttpPost]
        public IHttpActionResult Update(EventItem eventItem)
        {
            try
            {
                using (var es = new EventItemService())
                {
                    var myEvent = es.Add(eventItem);

                    var result = new ApiResult<EventItem>
                    {
                        Data = myEvent
                    };

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("delete/{id}")]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                using (var es = new EventItemService())
                {
                    es.Delete(id);

                    return Ok();
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }

        [Route("delete/event/{id}")]
        [HttpDelete]
        public IHttpActionResult DeleteByEvent(string id)
        {
            try
            {
                using (var es = new EventItemService())
                {
                    es.DeleteAllByEvent(id);

                    return Ok();
                }
            }
            catch (Exception ex)
            {
                //todo: Log ex
                var result = new ApiResult<string>
                {
                    Data = null,
                    Meta = new MetaResult
                    {
                        Errors = new List<Exception> { ex }
                    }
                };
                return Ok(result);
            }
        }
    }
}